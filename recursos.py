"""                           author: "Angel Morocho"
                       email: "angel.m.morocho.c@unl.edu.ec"

Ejercicio 1:
            Crear  un programa en el cual se permita crear una actividad que contenga al menos 3
            preguntas de selección en base al modelo de clases codificado en el item 1 presentado
            al final todo lo creado. Todo el código del proyecto debe estar en un proyecto en GitLab"""


datos = []
preguntas = []
opciones = []
respuestas = []


class actividades:
    identificador = " "
    nombre = " "
    cant_preguntas = 0 # Cantidad de preguntas a realizar

    def __init__(self ,nombre, identificador, cant_preguntas):
        self.identificador = nombre
        self.nombre = identificador
        self.cant_preguntas = cant_preguntas

    def datos_personales(self):
        nombre = input(" Ingrese su nombre: ")
        datos.append(nombre)
        identificador = input(" Ingrese N° de cedula: ")
        datos.append(identificador)
        print(datos)

    def generar_preguntas(self):
        for i in range(0, cant_preguntas): # Bucle para hacer las preguntas
            pregunta = input(f" Ingrese pregunta {i + 1}: ")
            preguntas.append(pregunta)
            for a in range(1, 4): # Bucle para ingresar opciones de respuesta
                opcion = input(f" Opción {a}: ")
                opciones.append(opcion)
            for r in range(1): # Bucle para ing. respuesta correcta de cada pregunta
                respuesta = input(" Ingrese respuesta correcta: ")
                respuestas.append(respuesta)

    def presentacion(self):
        print(" \n EL CUESTIONARIO QUE USTED CREO ES: \n")
        for i in range(0, self.cant_preguntas): # Bucle para presentar preguntas y respuestas
            print(f" >>> Pregunta {i + 1}: ", preguntas[i])
        for h in range(0, self.cant_preguntas + 1):
            print(f" opcion: {h + 1}", opciones[h])
        print(" Respuesta correcta: ", respuestas[i])

    def __str__(self):
        return f"mul {self.presentacion()}"


class PreguntaSeleccion(actividades):
    numero = 0
    texto = " "
    respuesta = ""

    def __init__(self, numero, texto, respuesta, nombre, identificador, cant_preguntas):
        super().__init__(nombre, identificador, cant_preguntas)
        self.numero = numero
        self.texto = texto
        self.respuesta = respuesta


cant_preguntas = int(input("Ingrese el numero de preguntas: "))
cuestionario = actividades("NOMBRE_AUTOR", "CEDULA_AUTOR", cant_preguntas)
cuestionario.generar_preguntas()
cuestionario.presentacion()